var udp = require('dgram');

var buffer = require('buffer');

// creating a client socket
var client = udp.createSocket('udp4');

//buffer msg
var data = Buffer.from('siddheshrane');

client.on('message',function(msg,info){
	console.log('Data received from server : ' + msg.toString());
	console.log('Received %d bytes from %s:%d\n',msg.length, info.address, info.port);
});

//sending msg
client.send(data,2222,'localhost',function(error){
	if(error){
	client.close();
	}else{
	console.log('Data sent !!!');
	}
});

var data1 = Buffer.from('hello');
var data2 = Buffer.from('world');

//sending multiple msg

var i;
for (i = 0; i < 10; i++) { 
	client.send([data1,data2],2222,'localhost',function(error){
	if(error){
		client.close();
	} else {
		console.log('Data sent !!!');
	}
	
	});
}

setTimeout(function(){
	client.close();
}, 10000);